package au.com.cmitinternational.androidiocwithcontentprovider.implementation;

import au.com.cmitinternational.androidiocwithcontentprovider.interfaces.IMainActivityViewModel;

/**
 * Created by Darcy on 19/10/2014.
 */
public class MainActivityViewModel implements IMainActivityViewModel {
    @Override
    public String getDetails(String name) {
        if (name.equals("Jill")) {
            return "Smart";
        }
        if (name.equals("Jack")) {
            return "Silly";
        }
        return null;
    }
}
