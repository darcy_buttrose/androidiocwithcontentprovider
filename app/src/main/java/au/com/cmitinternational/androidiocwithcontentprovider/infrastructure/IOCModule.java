package au.com.cmitinternational.androidiocwithcontentprovider.infrastructure;

import com.google.inject.Binder;
import com.google.inject.Module;

import au.com.cmitinternational.androidiocwithcontentprovider.implementation.MainActivityViewModel;
import au.com.cmitinternational.androidiocwithcontentprovider.interfaces.IMainActivityViewModel;

/**
 * Created by Darcy on 19/10/2014.
 */
public class IOCModule implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(IMainActivityViewModel.class).to(MainActivityViewModel.class);
    }
}
