package au.com.cmitinternational.androidiocwithcontentprovider.interfaces;

/**
 * Created by Darcy on 19/10/2014.
 */
public interface IMainActivityViewModel {
    String getDetails(String name);
}
